package aps2.hashmap;

import java.util.LinkedList;

/**
 * Hash map employing chaining on collisions.
 */
public class HashMapChaining {
	private LinkedList<Element> table[];
	private HashFunction.HashingMethod h;
	
	public HashMapChaining(int m, HashFunction.HashingMethod h) {
		this.h = h;
		this.table = new LinkedList[m];
		for (int i=0; i<table.length; i++) {
			table[i] = new LinkedList<Element>();
		}
	}
	
	public LinkedList<Element>[] getTable() {
		return this.table;
	}
	
	/**
	 * If the element doesn't exist yet, inserts it into the set.
	 * 
	 * @param k Element key
	 * @param v Element value
	 * @return true, if element was added; false otherwise.
	 */
	public boolean add(int k, String v) {
		if(! contains(k)) {
            Element element = new Element(k, v);
            int hashed;
            if(h == HashFunction.HashingMethod.DivisionMethod) {
                hashed = HashFunction.DivisionMethod(k, this.getTable().length);
            }else{
                hashed = HashFunction.KnuthMethod(k, this.getTable().length);
            }
            this.getTable()[hashed].add(element);
            return true;
        }else{
		    return false;
        }
	}

	/**
	 * Removes the element from the set.
	 * 
	 * @param k Element key
	 * @return true, if the element was removed; otherwise false
	 */
	public boolean remove(int k) {
        int hashed;
        if(h == HashFunction.HashingMethod.DivisionMethod) {
            hashed = HashFunction.DivisionMethod(k, this.getTable().length);
        }else{
            hashed = HashFunction.KnuthMethod(k, this.getTable().length);
        }
		for (Element e : this.getTable()[hashed]){
		    if (e.key == k){
		        this.getTable()[hashed].remove(e);
		        return true;
            }
        }
        return false;
	}

	/**
	 * Finds the element.
	 * 
	 * @param k Element key
	 * @return true, if the element was found; false otherwise.
	 */
	public boolean contains(int k) {
        int hashed;
        if(h == HashFunction.HashingMethod.DivisionMethod) {
            hashed = HashFunction.DivisionMethod(k, this.getTable().length);
        }else{
            hashed = HashFunction.KnuthMethod(k, this.getTable().length);
        }
        for (Element e : this.getTable()[hashed]){
            if (e.key == k) return true;
        }
        return false;
	}
	
	/**
	 * Maps the given key to its value, if the key exists in the hash map.
	 * 
	 * @param k Element key
	 * @return The value for the given key or null, if such a key does not exist.
	 */
	public String get(int k) {
        int hashed;
        if(h == HashFunction.HashingMethod.DivisionMethod) {
            hashed = HashFunction.DivisionMethod(k, this.getTable().length);
        }else{
            hashed = HashFunction.KnuthMethod(k, this.getTable().length);
        }
        for (Element e : this.getTable()[hashed]){
            if (e.key == k) return e.value;
        }
        return null;
	}
}

