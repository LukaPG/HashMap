package aps2.hashmap;

/**
 * Hash map with open addressing.
 */
public class HashMapOpenAddressing {
	private Element table[]; // table content, if element is not present, use Integer.MIN_VALUE for Element's key
	private HashFunction.HashingMethod h;
	private CollisionProbeSequence c;
	
	public static enum CollisionProbeSequence {
		LinearProbing,    // new h(k) = (h(k) + i) mod m
		QuadraticProbing, // new h(k) = (h(k) + i^2) mod m
		DoubleHashing     // new h(k) = (h(k) + i*h(k)) mod m
	};
	
	public HashMapOpenAddressing(int m, HashFunction.HashingMethod h, CollisionProbeSequence c) {
		this.table = new Element[m];
		this.h = h;
		this.c = c;
		
		// init empty slot as MIN_VALUE
		for (int i=0; i<m; i++) {
			table[i] = new Element(Integer.MIN_VALUE, "");
		}
	}

	public void doubleTable(){
	    Element[] newTable = new Element[this.getTable().length*2];
        for (int i=0; i<newTable.length; i++) {
            newTable[i] = new Element(Integer.MIN_VALUE, "");
        }
        for (Element e : this.getTable()){
            int hashed;
            if(h == HashFunction.HashingMethod.DivisionMethod) {
                hashed = HashFunction.DivisionMethod(e.key, newTable.length);
            }else{
                hashed = HashFunction.KnuthMethod(e.key, newTable.length);
            }

            if(newTable[hashed].equals(new Element(Integer.MIN_VALUE, ""))) {
                newTable[hashed] = e;
            }else{
                if(c == CollisionProbeSequence.LinearProbing){
                    int counter = 0;
                    while(! newTable[hashed].equals(new Element(Integer.MIN_VALUE, ""))){
                        hashed = (hashed + counter++) % newTable.length;
                        if (newTable[hashed].equals(new Element(Integer.MIN_VALUE, ""))){
                            newTable[hashed] = e;
                        }
                    }
                }
                else if (c == CollisionProbeSequence.QuadraticProbing){
                    int counter = 0;
                    while(! newTable[hashed].equals(new Element(Integer.MIN_VALUE, ""))){
                        hashed = (hashed + counter*counter++) % newTable.length;
                        if (newTable[hashed].equals(new Element(Integer.MIN_VALUE, ""))){
                            newTable[hashed] = e;
                        }
                    }
                }
                else if (c == CollisionProbeSequence.DoubleHashing){
                    int counter = 0;
                    while(! newTable[hashed].equals(new Element(Integer.MIN_VALUE, ""))){
                        hashed = (hashed + counter++*hashed) % newTable.length;
                        if (newTable[hashed].equals(new Element(Integer.MIN_VALUE, ""))){
                            newTable[hashed] = e;
                        }
                    }
                }
            }
        }
        this.table = newTable;
    }

    public boolean isFull(){
	    for(Element e : this.getTable()){
	        if (e.equals(new Element(Integer.MIN_VALUE, ""))){
	            return false;
            }
        }
        return true;
    }

	public Element[] getTable(){
		return this.table;
	}
	
	/**
	 * If the element doesn't exist yet, inserts it into the set.
	 * 
	 * @param k Element key
	 * @param v Element value
	 * @return true, if element was added; false otherwise.
	 */
	public boolean add(int k, String v) {
		if(! contains(k)){
			Element element = new Element(k, v);
			int hashed;
			if(h == HashFunction.HashingMethod.DivisionMethod) {
				hashed = HashFunction.DivisionMethod(k, this.getTable().length);
			}else{
				hashed = HashFunction.KnuthMethod(k, this.getTable().length);
			}
			if(this.getTable()[hashed].equals(new Element(Integer.MIN_VALUE, ""))){
			    this.getTable()[hashed] = element;
			    return true;
            }else{
			    if(isFull()){
			        doubleTable();
                }
			    if(c == CollisionProbeSequence.LinearProbing){
			        int counter = 1;
			        int OGhash  = hashed;
			        while(! this.getTable()[hashed].equals(new Element(Integer.MIN_VALUE, ""))){
                        hashed = (OGhash + counter)%this.getTable().length;
                        counter++;
                        if (this.getTable()[hashed].equals(new Element(Integer.MIN_VALUE, ""))){
                            this.getTable()[hashed] = element;
                            return true;
                        }
                    }
                }
                else if (c == CollisionProbeSequence.QuadraticProbing){
					int counter = 1;
                    int OGhash  = hashed;
					while(! this.getTable()[hashed].equals(new Element(Integer.MIN_VALUE, ""))){
						hashed = (OGhash + counter*counter) % this.getTable().length;
                        counter++;
						if (this.getTable()[hashed].equals(new Element(Integer.MIN_VALUE, ""))){
							this.getTable()[hashed] = element;
							return true;
						}
					}
				}
				else if (c == CollisionProbeSequence.DoubleHashing){
                    int counter = 1;
                    int OGhash  = hashed;
                    while(! this.getTable()[hashed].equals(new Element(Integer.MIN_VALUE, ""))){
                        hashed = (OGhash + counter*hashed) % this.getTable().length;
                        counter++;
                        if (this.getTable()[hashed].equals(new Element(Integer.MIN_VALUE, ""))){
                            this.getTable()[hashed] = element;
                            return true;
                        }
                    }
                }
            }
		}else{
		    return false;
        }
        return false;
	}

	/**
	 * Removes the element from the set.
	 * 
	 * @param k Element key
	 * @return true, if the element was removed; otherwise false
	 */
	public boolean remove(int k) {
		if (contains(k)){
            int hashed;
            if(h == HashFunction.HashingMethod.DivisionMethod) {
                hashed = HashFunction.DivisionMethod(k, this.getTable().length);
            }else{
                hashed = HashFunction.KnuthMethod(k, this.getTable().length);
            }
            if(this.getTable()[hashed].key == k) {
                this.getTable()[hashed] = new Element(Integer.MIN_VALUE, "");
                return true;
            }else{
                if(c == CollisionProbeSequence.LinearProbing){
                    int counter = 1;
                    int OGhash  = hashed;
                    while(this.getTable()[hashed].key != k){
                        hashed = (OGhash + counter)%this.getTable().length;
                        counter++;
                        if (this.getTable()[hashed].key == k){
                            this.getTable()[hashed] = new Element(Integer.MIN_VALUE, "");
                            return true;
                        }
                    }
                }
                else if (c == CollisionProbeSequence.QuadraticProbing){
                    int counter = 1;
                    int OGhash  = hashed;
                    while(this.getTable()[hashed].key != k){
                        hashed = (OGhash + counter*counter) % this.getTable().length;
                        counter++;
                        if (this.getTable()[hashed].key == k){
                            this.getTable()[hashed] = new Element(Integer.MIN_VALUE, "");
                            return true;
                        }
                    }
                }
                else if (c == CollisionProbeSequence.DoubleHashing){
                    int counter = 1;
                    int OGhash  = hashed;
                    while(this.getTable()[hashed].key != k){
                        hashed = (OGhash + hashed*counter) % this.getTable().length;
                        counter++;
                        if (this.getTable()[hashed].key == k){
                            this.getTable()[hashed] = new Element(Integer.MIN_VALUE, "");
                            return true;
                        }
                    }
                }
            }
        }else{
		    return false;
        }
        return false;
	}

	/**
	 * Finds the element.
	 * 
	 * @param k Element key
	 * @return true, if the element was found; false otherwise.
	 */
	public boolean contains(int k) {
        int hashed;
        if(h == HashFunction.HashingMethod.DivisionMethod) {
            hashed = HashFunction.DivisionMethod(k, this.getTable().length);
        }else{
            hashed = HashFunction.KnuthMethod(k, this.getTable().length);
        }
        return this.getTable()[hashed].key == k;
	}
	
	/**
	 * Maps the given key to its value, if the key exists in the hash map.
	 * 
	 * @param k Element key
	 * @return The value for the given key or null, if such a key does not exist.
	 */
	public String get(int k) {
        int hashed;
        if(h == HashFunction.HashingMethod.DivisionMethod) {
            hashed = HashFunction.DivisionMethod(k, this.getTable().length);
        }else{
            hashed = HashFunction.KnuthMethod(k, this.getTable().length);
        }
        return this.getTable()[hashed].value;
	}
}

